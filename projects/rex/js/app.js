const room = [
  0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0,
  1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1,
  0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1
];

room.forEach(item => {
  let bench = document.getElementById('bench');
  let chair = document.createElement('img');
  if (item == 1) {
    chair.src = "assets/img/chair-busy.svg";
    chair.classList.add('chair', 'chair-busy');
  } else {
    chair.src = "assets/img/chair-unavailable.svg";
    chair.classList.add('chair', 'chair-unavailable');
  }
  bench.appendChild(chair);
});
