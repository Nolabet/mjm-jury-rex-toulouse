<!DOCTYPE html>
<html lang="fr" dir="ltr">

  <?php require_once('partials/head.php'); ?>

  <body>

    <script>
      AOS.init();
    </script>

    <?php require_once('partials/menu.php'); ?>

    <main class="content">
      <header class="hero">
        <div class="logo d-flex pt-1 pl-3">
          <a href="home.php"><img src="assets/img/logo-le-rex-toulouse.svg" alt=""></a>
        </div>

        <?php require_once('partials/carousel.php'); ?>

        <?php require_once('partials/scrolldown.php'); ?>

        <div class="alert d-block text-center">
          <h1 class="pt-3 uppercase">Réouverture le 19 mai</h1>
        </div>
      </header>
      <section class="pt-2 pb-3" id="main-covid">
        <div class="row" data-aos="fade-down" data-aos-duration="800">
          <div class="block-max col-50">
            <div class="card shadow-sm text-center py-4 border-sm">
              <h3 class="uppercase">2 sièges de distanciation sociale</h3>
              <div class="bench d-inline-block mt-3" id="bench"></div>
              <p class="leg mt-2"><span class="leg-busy"><svg height="8" width="8">
                    <circle cx="4" cy="4" r="4" stroke-width="3" fill="#1B1B1B" />
                    Sorry, your browser does not support inline SVG.
                  </svg></span> Chaise occupé <span class="leg-unavailable ml-3"><svg height="8" width="8">
                    <circle cx="4" cy="4" r="4" stroke-width="3" fill="#EFCA1D" />
                    Sorry, your browser does not support inline SVG.
                  </svg></span> Distanciation sociale.</p>
              <p class="mt-3">Le port du masque est obligatoire dans tous nos espaces<br>(y compris en salle).</p>
            </div>
          </div>
          <div class="block-max col-50">
            <p class="my-3 pr-8" data-aos="fade-up" data-aos-duration="800" data-aos-delay="400"><strong>Dès le 19 mai</strong>, nous pourrons de nouveau vous recevoir. Vous pourrez profiter de notre salle de spectacle récemment refaite à neuf
              !<br><br>
              Seulement, nous devons limiter l'accueil à <strong>35% de la capacité de la salle</strong>. Le 9 juin, la jauge sera repensée à la hausse pour atteindre 65% et enfin, le 30 juin, elle disparaîtra. Seuls resteront les gestes barrière et
              la distanciation
              physique.</p>
            <div class="" data-aos="fade-right" data-aos-duration="800" data-aos-delay="500">
              <a class="btn-sm btn-dark" href="#">En savoir plus <i class="bi bi-arrow-right-short"></i></a>
            </div>
          </div>
        </div>
      </section>
      <section class="pt-2 pb-4" id="room">
        <!-- La salle -->
      </section>

    <?php require_once('partials/footer.php'); ?>

    <script type="text/javascript" src="js/app.js"></script>
    <!-- <script type="text/javascript" src="js/shows.js"></script> -->

  </body>

</html>
