<!DOCTYPE html>
<html lang="fr" dir="ltr">

  <?php require_once('partials/head.php'); ?>

  <body>

    <script>
      AOS.init();

    </script>

    <?php require_once('partials/menu.php'); ?>

    <main class="content">
      <div class="background"></div>
      <div class="row">
        <div class="col-33">
          <div class="curve program--card combichrist">
            <div class="footer--card">
              <div class="connections">
                <div class="connection date text-center ml-2">
                  <div class="desc">28 juillet 2021</div>
                </div>
                <div class="connection hour text-center">
                  <div class="desc">21h</div>
                </div>
              </div>
              <svg class="curve">
                <path id="p" d="M0,200 Q80,100 400,200 V150 H0 V50" transform="translate(0 300)" />
                <rect id="dummyRect" x="0" y="0" height="450" width="400" fill="transparent" />
                <!-- slide up-->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,100 400,50 V150 H0 V50" fill="freeze" begin="dummyRect.mouseover" end="dummyRect.mouseout" dur="0.1s" id="bounce1" />
                <!-- slide up and curve in -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,0 400,50 V150 H0 V50" fill="freeze" begin="bounce1.end" end="dummyRect.mouseout" dur="0.15s" id="bounce2" />
                <!-- slide down and curve in -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,80 400,50 V150 H0 V50" fill="freeze" begin="bounce2.end" end="dummyRect.mouseout" dur="0.15s" id="bounce3" />
                <!-- slide down and curve out -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,45 400,50 V150 H0 V50" fill="freeze" begin="bounce3.end" end="dummyRect.mouseout" dur="0.1s" id="bounce4" />
                <!-- curve in -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,50 400,50 V150 H0 V50" fill="freeze" begin="bounce4.end" end="dummyRect.mouseout" dur="0.05s" id="bounce5" />

                <animate xlink:href="#p" attributeName="d" to="M0,200 Q80,100 400,200 V150 H0 V50" fill="freeze" begin="dummyRect.mouseout" dur="0.15s" id="bounceOut" />
              </svg>
              <div class="desc ml-2">
                <div class="name">Combichrist</div>
                <div class="job">Rock music</div>
              </div>
            </div>
            <div class="card-blur"></div>
          </div>
        </div>
        <div class="col-33">
          <div class="curve program--card michel">
            <div class="footer--card">
              <div class="connections">
                <div class="connection date text-center ml-2">
                  <div class="desc">28 juillet 2021</div>
                </div>
                <div class="connection hour text-center">
                  <div class="desc">21h</div>
                </div>
              </div>
              <svg class="curve2">
                <path id="p" d="M0,200 Q80,100 400,200 V150 H0 V50" transform="translate(0 300)" />
                <rect id="dummyRect" x="0" y="0" height="450" width="400" fill="transparent" />
                <!-- slide up-->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,100 400,50 V150 H0 V50" fill="freeze" begin="dummyRect.mouseover" end="dummyRect.mouseout" dur="0.1s" id="bounce1" />
                <!-- slide up and curve in -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,0 400,50 V150 H0 V50" fill="freeze" begin="bounce1.end" end="dummyRect.mouseout" dur="0.15s" id="bounce2" />
                <!-- slide down and curve in -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,80 400,50 V150 H0 V50" fill="freeze" begin="bounce2.end" end="dummyRect.mouseout" dur="0.15s" id="bounce3" />
                <!-- slide down and curve out -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,45 400,50 V150 H0 V50" fill="freeze" begin="bounce3.end" end="dummyRect.mouseout" dur="0.1s" id="bounce4" />
                <!-- curve in -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,50 400,50 V150 H0 V50" fill="freeze" begin="bounce4.end" end="dummyRect.mouseout" dur="0.05s" id="bounce5" />

                <animate xlink:href="#p" attributeName="d" to="M0,200 Q80,100 400,200 V150 H0 V50" fill="freeze" begin="dummyRect.mouseout" dur="0.15s" id="bounceOut" />
              </svg>
              <div class="desc ml-2">
                <div class="name">Combichrist</div>
                <div class="job">Rock music</div>
              </div>
            </div>
            <div class="card-blur"></div>
          </div>
        </div>
        <div class="col-33">
          <div class="curve program--card amenra">
            <div class="footer--card">
              <div class="connections">
                <div class="connection date text-center ml-2">
                  <div class="desc">28 juillet 2021</div>
                </div>
                <div class="connection hour text-center">
                  <div class="desc">21h</div>
                </div>
              </div>
              <svg class="curve3">
                <path id="p" d="M0,200 Q80,100 400,200 V150 H0 V50" transform="translate(0 300)" />
                <rect id="dummyRect" x="0" y="0" height="450" width="400" fill="transparent" />
                <!-- slide up-->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,100 400,50 V150 H0 V50" fill="freeze" begin="dummyRect.mouseover" end="dummyRect.mouseout" dur="0.1s" id="bounce1" />
                <!-- slide up and curve in -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,0 400,50 V150 H0 V50" fill="freeze" begin="bounce1.end" end="dummyRect.mouseout" dur="0.15s" id="bounce2" />
                <!-- slide down and curve in -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,80 400,50 V150 H0 V50" fill="freeze" begin="bounce2.end" end="dummyRect.mouseout" dur="0.15s" id="bounce3" />
                <!-- slide down and curve out -->
                  <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,45 400,50 V150 H0 V50" fill="freeze" begin="bounce3.end" end="dummyRect.mouseout" dur="0.1s" id="bounce4" />
                <!-- curve in -->
                <animate xlink:href="#p" attributeName="d" to="M0,50 Q80,50 400,50 V150 H0 V50" fill="freeze" begin="bounce4.end" end="dummyRect.mouseout" dur="0.05s" id="bounce5" />

                <animate xlink:href="#p" attributeName="d" to="M0,200 Q80,100 400,200 V150 H0 V50" fill="freeze" begin="dummyRect.mouseout" dur="0.15s" id="bounceOut" />
              </svg>
              <div class="desc ml-2">
                <div class="name">Combichrist</div>
                <div class="job">Rock music</div>
              </div>
            </div>
            <div class="card-blur"></div>
          </div>
        </div>
      </div>
    </main>

    <!-- <script type="text/javascript" src="js/app.js"></script> -->
    <!-- <script type="text/javascript" src="js/shows.js"></script> -->

  </body>

</html>
