<div class='scrolldown' id="scrolldown">
  <div class="chevrons">
    <div class='chevrondown'></div>
    <div class='chevrondown'></div>
  </div>
</div>

<script>

  // When the user scrolls the page, execute myFunction
  window.onscroll = function() {myFunction()};

  function myFunction() {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    if (winScroll >50) {
      document.getElementById("scrolldown").style.transform = "translateY(50px)";
      document.getElementById("scrolldown").style.opacity = 0;
    } else {
      document.getElementById("scrolldown").style.transform = "translateY(-50px)";
      document.getElementById("scrolldown").style.opacity = 0.9;
    }
  }

</script>
