<script type="module">
  import Swiper from 'https://unpkg.com/swiper/swiper-bundle.esm.browser.min.js'

  const swiper = new Swiper('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      // type: "progressbar",
    },

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
      el: '.swiper-scrollbar',
    },
  });
</script>


<!-- Slider main container -->
<div class="swiper-container" data-aos="fade-right" data-aos-duration="800" data-aos-delay="500">
  <!-- Additional required wrapper -->
  <div class="swiper-wrapper p-5">
    <!-- Slides -->
    <div class="swiper-slide" data>
      <text class="">28 juillet 2021</text>
      <h1 class="swiper-artist mt-2">Combichrist</h1>
      <p><strong>Combichrist</strong> en plein juillet, ça vous tente ?<br>Nous, en tout cas, en attendant de pouvoir suer de nouveau dans les pogos, tout en braillant des refrains de gros bourrin débiles ! Nous vous donnons rendez-vous avec la bande d'Andy Laplegua, qui donnerapour un concert de reprise à ne pas louper grâce à Garmonbozia ! </p>
      <a class="btn-sm btn-white mt-4" href="agenda.php">Réserver <i class="bi bi-arrow-right-short"></i></a>
    </div>
    <div class="swiper-slide">
      <text class="">21-22 septembre 2021</text>
      <h1 class="swiper-artist mt-2">DVNE & Déluge</h1>
      <p>Le groupe britannique de Prog / Sludge <strong>DVNE</strong> sillonnera l'Europe cet automne afin de présenter son nouvel album "Etemen Ænka". A ses côtés, le sextet français <strong>DÉLUGE</strong> vous immergera dans leurs univers mêlant Black Metal Ambiant et Post-Hardcore. C'est avec grand plaisir que nous accueillerons le 21 et 22 septembre au Rex de toulouse ces deux groupes renouvelant chacun à sa manière le paysage Metal européen.</p>
      <a class="btn-sm btn-white mt-4" href="agenda.php">Réserver <i class="bi bi-arrow-right-short"></i></a>
    </div>
    <div class="swiper-slide">
      <text class="">1er octobre 2021</text>
      <h1 class="swiper-artist mt-2">Amenra</h1>
      <p><strong>Amenra</strong> est un groupe belge de doom metal, originaire de Courtrai, en Région flamande.<br>Il est formé en 1999 par le chanteur Colin H. van Eeckhout et le guitariste Mathieu Vandekerckhove, et plus tard rejoint par le batteur Bjorn Lebon, le guitariste Lennart Bossu et le bassiste Levy Seynaeve. Nous aurons le plaisir de les accueillir pour leur passage en France cet automne !</p>
      <a class="btn-sm btn-white mt-4" href="agenda.php">Réserver <i class="bi bi-arrow-right-short"></i></a>
    </div>
  </div>

  <!-- If we need navigation buttons -->
  <!-- <div class="swiper-button-prev"></div> -->
  <div class="swiper-button-next"></div>

  <!-- If we need pagination -->
  <!-- <div class="ml-1 swiper-pagination"></div> -->

  <!-- If we need scrollbar -->
  <!-- <div class="swiper-scrollbar"></div> -->
</div>
