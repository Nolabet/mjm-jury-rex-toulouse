<section class="py-8 mt-2" id="partners">
  <h4 class="uppercase text-center">Partenaires</h4>
  <div class="container mt-3">
    <div class="row text-center">
      <div class="col-20">
        <img class="partner-v" src="assets/img/teuf.svg" alt="">
      </div>
      <div class="col-20">
        <img class="partner-h" src="assets/img/festik.svg" alt="">
      </div>
      <div class="col-20">
        <img class="partner-v" src="assets/img/region.svg" alt="">
      </div>
      <div class="col-20">
        <img class="partner-v" src="assets/img/crouzil-boissons.svg" alt="">
      </div>
      <div class="col-20">
        <img class="partner-h" src="assets/img/logofmr.svg" alt="">
      </div>
    </div>
  </div>
</section>
</main>

<footer class="">
  <div class="row text-center text-dark footer--nav d-grid">
    <ul class="pt-2">
      <li class="d-inline-block mx-3"><a href="#">Home</a></li>
      <li class="d-inline-block mx-3"><a href="#">Agenda</a></li>
      <li class="d-inline-block mx-3"><a href="#">Le Rex</a></li>
      <li class="d-inline-block mx-3"><a href="#">Privatisation</a></li>
      <li class="d-inline-block mx-3"><a href="#">Pros</a></li>
      <li class="d-inline-block mx-3"><a href="#">Contact</a></li>
    </ul>
  </div>
  <div class="row footer--info pt-5">
    <div class="row text-white justify-content-center">
      <div class="col-20 pl-3">
        <img src="assets/img/logo-le-rex-toulouse.svg" alt="">
      </div>
      <div class="col-20 pl-3">
        <p>Le Rex Toulouse<br>
          15 avenue Honoré Serres<br>
          31000 Toulouse</p>
      </div>
      <div class="col-20 pl-3">
        <a href="tel:0561385771">05 61 38 57 71</a>
        <a href="mailto:contact@lerextoulouse.com">contact@lerextoulouse.com</a>
      </div>
      <div class="col-20 pl-3">
        <p>Suivez-nous !</p>
        <div class="">
          <img class="social-media" src="assets/img/instagram-brands.svg" alt="">
          <img class="social-media" src="assets/img/facebook-brands.svg" alt="">
        </div>
      </div>
    </div>
    <div class="row text-center text-min">
      <p>©Copyright 2021 Le Rex Toulouse. Tous droits reversés.</p>
    </div>
  </div>
</footer>
