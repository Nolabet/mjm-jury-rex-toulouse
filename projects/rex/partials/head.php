<head>
  <meta charset="utf-8">
  <meta name="author" content="Nicolas Blet" />
  <link rel="stylesheet" href="css/master.css">
  <meta name="description" content="Le Rex Toulouse" />
  <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  <title>Le Rex Toulouse</title>
</head>
