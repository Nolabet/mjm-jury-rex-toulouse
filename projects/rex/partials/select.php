<section class="mx-3 p-2 mb-3 mt-n5 select shadow-sm">
  <details class="custom-select">
    <summary class="radios">
      <input type="radio" name="item" id="default" title="Toutes catégories" checked>
      <input type="radio" name="item" id="item1" title="Comédie">
      <input type="radio" name="item" id="item2" title="Théatre">
      <input type="radio" name="item" id="item3" title="Performance">
      <input type="radio" name="item" id="item4" title="Concert">
    </summary>
    <ul class="list shadow-xl">
      <li><label for="item1">Comédie</label></li>
      <li><label for="item2">Théatre</label></li>
      <li><label for="item3">Performance</label></li>
      <li><label for="item4">Concert</label></li>
      <li><label for="default">Toutes catégories</label></li>
    </ul>
  </details>
  <details class="custom-select">
    <summary class="radios">
      <input type="radio" name="period" id="default2" title="Toutes les dates" checked>
      <input type="radio" name="period" id="period1" title="Demain">
      <input type="radio" name="period" id="period2" title="Cette semaine">
      <input type="radio" name="period" id="period3" title="Ce mois-ci">
      <input type="radio" name="period" id="period4" title="Cette année">
      <input type="radio" name="period" id="period5" title="Saison 2022">
    </summary>
    <ul class="list shadow-xl">
      <li><label for="period1">Demain</label></li>
      <li><label for="period2">Cette semaine</label></li>
      <li><label for="period3">Ce mois-ci</label></li>
      <li><label for="period4">Cette année</label></li>
      <li><label for="default2">Toutes les dates</label></li>
    </ul>
  </details>
  <details class="custom-select">
    <summary class="radios">
      <input type="radio" name="type" id="default3" title="Tous publics" checked>
      <input type="radio" name="type" id="type1" title="Enfants">
      <input type="radio" name="type" id="type2" title="13-17 ans">
      <input type="radio" name="type" id="type3" title="Adultes">
    </summary>
    <ul class="list shadow-xl">
      <li><label for="type1">Enfants</label></li>
      <li><label for="type2">13-17 ans</label></li>
      <li><label for="type3">Adultes</label></li>
      <li><label for="default3">Tous publics</label></li>
    </ul>
  </details>
  <a class="btn-sm btn-cta ml-3" href="#results" disabled>Rechercher <i class="bi bi-arrow-right-short"></i></a>
  <!-- <div class="div ml-5 flex-end">
    <div class="d-inline" onclick="toDisplay100()">
      <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="#EFCA1D" class="bi bi-view-stacked" viewBox="0 0 16 16">
        <path d="M3 0h10a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3zm0 8h10a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1H3z"/>
      </svg>
    </div>
    <div class="d-inline ml-1" onclick="toDisplay50()">
      <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-grid" viewBox="0 0 16 16">
        <path d="M1 2.5A1.5 1.5 0 0 1 2.5 1h3A1.5 1.5 0 0 1 7 2.5v3A1.5 1.5 0 0 1 5.5 7h-3A1.5 1.5 0 0 1 1 5.5v-3zM2.5 2a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 1h3A1.5 1.5 0 0 1 15 2.5v3A1.5 1.5 0 0 1 13.5 7h-3A1.5 1.5 0 0 1 9 5.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zM1 10.5A1.5 1.5 0 0 1 2.5 9h3A1.5 1.5 0 0 1 7 10.5v3A1.5 1.5 0 0 1 5.5 15h-3A1.5 1.5 0 0 1 1 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 9h3a1.5 1.5 0 0 1 1.5 1.5v3a1.5 1.5 0 0 1-1.5 1.5h-3A1.5 1.5 0 0 1 9 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3z"/>
      </svg>
    </div>
  </div> -->
</section>
<div class="px-5 mb-5">
  <h4>*La programmation est subceptible d'être modifiée selon les annonces gouvernementales.</h4>
</div>


<!-- Work in progress -->

<script>

  function toDisplay50 () {
    var grid = document.getElementsByClassName('hover');
    grid.classList.remove("w-100");
    grid.classList.add("w-50");
  }

  function toDisplay100 () {
    var grid = document.getElementsByClassName('hover');
    grid.classList.remove("w-50");
    grid.classList.add("w-100");
  }

</script>
