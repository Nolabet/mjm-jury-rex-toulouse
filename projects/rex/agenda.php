<!DOCTYPE html>
<html lang="fr" dir="ltr">

  <?php require_once('partials/head.php'); ?>

  <body>

    <script>
      AOS.init();
    </script>

    <?php require_once('partials/menu.php'); ?>

    <main class="content">
      <header class="agenda">
        <div class="logo d-flex pt-1 pl-3">
          <a href="home.php"><img src="assets/img/logo-le-rex-toulouse.svg" alt=""></a>
        </div>
        <div class="row parallax">
          <h1 class="ml-3 mb-5"  id="parallax" data-aos="zoom-out"></h1>
        </div>
      </header>

      <?php require_once('partials/select.php'); ?>

      <section class="p-5 mb-5">
        <div class="display" id="results">
        	<div class="hover" data-aos="zoom-in-up" data-aos-duration="12000" data-aos-anchor-placement="center-bottom">
            <span></span>
            <div class="col-50">
              <div class="text">
                <h2>Combichrist</h2>
                <h4>28 juillet 2021 • 21h</h4>
                <p><strong>Combichrist</strong> en plein juillet, ça vous tente ?<br>Nous, en tout cas, en attendant de pouvoir suer de nouveau dans les pogos, tout en braillant des refrains de gros bourrin débiles ! Nous vous donnons rendez-vous avec la bande d'Andy Laplegua, qui donnerapour un concert de reprise à ne pas louper grâce à Garmonbozia ! </p>
              </div>
            </div>
            <div class="col-50 p-3 price">
              <h2 class="text-white">17€/pers.</h2>
              <a class="btn-sm btn-white" href="#">Réserver <i class="bi bi-arrow-right-short"></i></a>
            </div>
        	</div>
        	<div class="hover filled" data-aos="zoom-in-up" data-aos-duration="12000" data-aos-anchor-placement="center-bottom">
        		<span></span>
            <div class="col-50">
              <div class="text">
                <h2>DVNE & DELUGE</h2>
                <h4>21-22 septembre 2021 • 21h</h4>
                <p>Le groupe britannique de Prog / Sludge <strong>DVNE</strong> sillonnera l'Europe cet automne afin de présenter son nouvel album "Etemen Ænka". A ses côtés, le sextet français <strong>DÉLUGE</strong> vous immergera dans leurs univers mêlant Black Metal Ambiant et Post-Hardcore. C'est avec grand plaisir que nous accueillerons le 21 et 22 septembre au Rex de toulouse ces deux groupes renouvelant chacun à sa manière le paysage Metal européen.</p>
              </div>
            </div>
            <div class="col-50 p-3 price">
              <h2 class="text-white">23€/pers.</h2>
              <a class="btn-sm btn-filled" href="#">Complet</a>
            </div>
        	</div>
        	</div>
        	<div class="hover" data-aos="zoom-in-up" data-aos-duration="12000" data-aos-anchor-placement="center-bottom">
        		<span></span>
            <div class="col-50">
              <div class="text">
                <h2>AMENRA TOUR 2021</h2>
                <h4>1er octobre 2021 - 21h</h4>
                <p><strong>Amenra</strong> est un groupe belge de doom metal, originaire de Courtrai, en Région flamande.<br>Il est formé en 1999 par le chanteur Colin H. van Eeckhout et le guitariste Mathieu Vandekerckhove, et plus tard rejoint par le batteur Bjorn Lebon, le guitariste Lennart Bossu et le bassiste Levy Seynaeve. Nous aurons le plaisir de les accueillir pour leur passage en France cet automne !</p>
              </div>
            </div>
            <div class="col-50 p-3 price">
              <h2 class="text-white">27€/pers.</h2>
              <a class="btn-sm btn-white" href="#">Réserver <i class="bi bi-arrow-right-short"></i></a>
            </div>
        	</div>
        	<div class="hover" data-aos="zoom-in-up" data-aos-duration="12000" data-aos-anchor-placement="center-bottom">
        		<span></span>
            <div class="col-50">
              <div class="text">
                <h2>Michel</h2>
                <h4>28 octobre 2021 - 21h</h4>
                <p><strong>Michel</strong> est aussi unique que son patronyme semble familier.
                  Tout comme les vrais espions, sa musique vient du froid: un bloc de deep house venu d’Europe de l’Est percé par un rap français, dans lequel il a choisi de placer sa foi. Un style en dessous de zéro qui l’installe déjà comme le futur de l’électro, au pays des antihéros.
                  De son éducation musicale il retient le jeu et ses mots, une diction unique, arme personnelle pour développer son storytelling qui fait s’entrechoquer les histoires du quotidien. Le tout dans des virages serrés, Souvent au second degré, à l’image du titre “Mal Agi”, manifeste en hommage aux défaites, ou encore “J’me barre”, comme le point d’une ligne de fuite dont il est difficile de trouver la sortie.
                  Sa musicalité et son sens du détail pourraient déjà le mener vers les comparaisons, reflet potentiel de la nouvelle garde du rap français. Mais attention aux analogies faites à la hâte. Michel disperse toutes envies de rapprochements, si ce n’est lors d’une nuit en solitaire, justement. Sur un dancefloor aux allures de mur de fer, où une tension synthétique teintée de mélancolie éclairent la suite. Écrite en cyrillique.</p>
              </div>
            </div>
            <div class="col-50 p-3 price">
              <h2 class="text-white">18€/pers.</h2>
              <a class="btn-sm btn-white" href="#" disabled>Réserver <i class="bi bi-arrow-right-short"></i></a>
            </div>
        	</div>
        </div>
      </section>
    </main>

    <?php require_once('partials/footer.php'); ?>

    <script>

    var current_date = new Date();

    console.log(current_date);

    var year  = current_date.getFullYear();
    var month = current_date.getMonth();
    var day   = current_date.getDate();

    console.log(year);
    console.log(month);
    console.log(day);

    switch (month) {
      case 0:
        month = "janvier";
        break;
      case 1:
        month = "février";
        break;
      case 2:
        month = "mars";
        break;
      case 3:
        month = "avril";
        break;
      case 4:
        month = "mai";
        break;
      case 5:
        month = "juin";
        break;
      case 6:
        month = "juillet";
        break;
      case 7:
        month = "aout";
        break;
      case 8:
        month = "septembre";
        break;
      case 9:
        month = "octobre";
        break;
      case 10:
        month = "nowembre";
        break;
      case 11:
        month = "décembre";
    }

    switch (day) {
      case 0:
        day = "Dimanche";
        break;
      case 1:
        day = "Lundi";
        break;
      case 2:
         day = "Mardi";
        break;
      case 3:
        day = "Mercredi";
        break;
      case 4:
        day = "Jeudi";
        break;
      case 5:
        day = "Vendredi";
        break;
      case 6:
        day = "Samedi";
    }

    month = checkZero(month);
    day   = checkZero(day);

    var date = "";

    date += day;
    date += " ";
    date += month;
    date += " ";
    date += year;

    document.querySelector("#parallax").innerHTML = date;

    function checkZero(i)
    {
      if (i < 10)
      {
          i = "0" + i
      };  // add zero in front of numbers < 10

      return i;
    }

      // When the user scrolls the page, execute myFunction
      window.onscroll = function() {myFunction()};

      function myFunction() {
        var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
        if (winScroll >50) {
          document.getElementById("parallax").style.transform = "translateY(50px)";
          document.getElementById("parallax").style.opacity = 0;
        } else {
          document.getElementById("parallax").style.transform = "translateY(-50px)";
          document.getElementById("parallax").style.opacity = 0.9;
        }
      }

    </script>

    <!-- <script type="text/javascript" src="js/app.js"></script> -->
    <!-- <script type="text/javascript" src="js/shows.js"></script> -->

  </body>

</html>
