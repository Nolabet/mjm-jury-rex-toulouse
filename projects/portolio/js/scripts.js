AOS.init();

const menu = document.querySelector('.menu');
const ul = document.querySelector('.item-list');
const btn = menu.querySelector('.nav-tgl');
btn.addEventListener('click', evt => {
  menu.classList.toggle('active');
  ul.classList.toggle('active');
})

window.addEventListener('click', evt => {
  console.log(evt.clientX);
  console.log(evt.clientY);
  console.log("----");
})

var sizes = [
  'xs',
  'sm',
  'md',
  'lg',
];

for (var i = 0; i < myprojects.length; i++) {
  var random_size = Math.floor(Math.random() * sizes.length)
  console.log(sizes[random_size]);
  createNewCircle(sizes[random_size], i)
}

createSummary(myprojects);
createAllProjects(myprojects);

// ----------------------------------------------------------------------------

function createNewCircle(size, i) {

  let section = document.getElementById("animation_circle");
  var new_circle = document.createElement('div');
  var circle_id = "circle-" + i;
  var circle_size = "circle-" + size;

  new_circle.classList.add("circle", circle_size);
  new_circle.setAttribute('id', circle_id);
  new_circle.setAttribute('data-aos', "zoom-out");
  new_circle.setAttribute('data-aos-duration', "800");
  new_circle.setAttribute('data-aos-delay', "500");
  new_circle.setAttribute('data-aos-anchor', "#animation_circle");

  // translate(X, Y)
  let move = "translate(" + (Math.floor(Math.random() * 400) - 200) + "px," + (Math.floor(Math.random() * 300) - 200) + "px)";

  console.log(move);

  new_circle.style.transform = move;

  console.log(new_circle);
  section.appendChild(new_circle);

}


function createSummary(array) {

  let section = document.getElementById("summary");
  var container = document.createElement('div');
  container.classList.add("container", "shootingStar__list");

  var title = '<p class="project__header solid" data-aos="fade-right" data-aos-duration="800" data-aos-delay="100"><span class="outline">My</span>Projects</p>';
  container.innerHTML = title;
  section.appendChild(container);

  let aos_delay = 100;

  for (var i = 0; i < array.length; i++) {
    var new_project = document.createElement('div');
    new_project.classList.add("shootingStar", array[i].class);
    new_project.setAttribute('data-aos', "fade-up-left");
    new_project.setAttribute('data-aos-duration', "800");
    new_project.setAttribute('data-aos-delay', aos_delay);
    aos_delay = aos_delay + 100;
    var new_projectContent = '<a href="#' + array[i].anchor + '"';
    new_projectContent += '<span class="shootingStar__content">';
    new_projectContent += '<span class="shootingStar__num-text"> 0' + array[i].id + ' • ' + array[i].short_title + '</span></span>';
    new_projectContent += '</a>';

    new_project.innerHTML = new_projectContent;
    container.appendChild(new_project);
  }
}

function createAllProjects(array) {

  let section = document.getElementById("projects");

  for (var i = 0; i < array.length; i++) {

    console.log('### ' + array[i].short_title);

    var new_project = document.createElement('div');
    new_project.classList.add("col-2", "p-5");
    new_project.setAttribute('id', array[i].anchor);

    var col_one = document.createElement('div');
    col_one.classList.add("col-one");
    col_one.setAttribute('data-aos', "fade-right");
    col_one.setAttribute('data-aos-duration', "800");
    // --------------------------------------------
    var col_oneContent = '<div class="row">';
    col_oneContent += '<img class="project__img" src=".' + array[i].url + 'album-5.jpg" alt="Mockup de projet">';
    col_oneContent += '</div>';
    // --------------------------
    col_one.innerHTML = col_oneContent;
    new_project.appendChild(col_one);

    var col_two = document.createElement('div');
    col_two.classList.add("col-two", "px-4");
    col_two.setAttribute('data-aos', "fade-left");
    col_two.setAttribute('data-aos-duration', "800");
    col_two.setAttribute('data-aos-delay', "200");
    // -------------------------------------
    var col_twoContent = '<div class="row">';
    col_twoContent += '<span class="project__duration">' + array[i].short_desc + '</span>';
    col_twoContent += '<h2 class="mt-2">0' + array[i].id + ' • ' + array[i].short_title + '</h2>';
    col_twoContent += '<p class="mt-1">' + array[i].description + '</p>';
    col_twoContent += '<p class="mt-2">' + array[i].client + ' • ' + array[i].date + ' • ' + array[i].duration + '</p>';
    col_twoContent += '</div>';
    col_twoContent += '<div class="row col-2 mt-3">';
    col_twoContent += '<div class="col-one"><img class="project__img" src=".' + array[i].url + 'album-6.jpg" alt="Mockup de projet"></div>';
    col_twoContent += '<div class="col-two"><img class="project__img" src=".' + array[i].url + 'album-7.jpg" alt="Mockup de projet"></div>';
    col_twoContent += '</div>';
    //  --------------------------
    col_two.innerHTML = col_twoContent;
    new_project.appendChild(col_two);

    console.log(new_project);
    section.appendChild(new_project);

  }
}
