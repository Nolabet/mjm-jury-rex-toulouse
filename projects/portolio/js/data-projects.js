const myprojects = [{
    id: 1,
    url: '/img/tattoo/',
    img: 'tattoo-main.jpg',
    anchor: 'tattoo',
    class: 'tattoo-card',
    date: "20/12/2020",
    short_title: "Tattoo Studio",
    short_desc: "Showcase website",
    description: 'This is a landing page dedicated to a tattoo parlor. Through the identity of the tattoo artist, I created a real showcase allowing him to present his activity, but also to create a point of contact with his customers and future customers. The tattoos are used as graphic bases, followed by the Nova Flat typography.',
    client: "Rackay Frédéric",
    duration: "3 weeks"
  },
  {
    id: 2,
    url: '/img/philemon/',
    img: 'philemon.jpg',
    anchor: 'philemon',
    class: 'philemon-card',
    date: "02/12/2020",
    short_title: "Philemon",
    short_desc: "E-Commerce website",
    description: 'The Philemon estate is a French vineyard in the south of France. Its activity is remarkable but remains limited because of its poor communication. Its website is very rustic, as well as the packaging of its bottles. This is why I propose a global rebranding, with a new visual identity and a new website.',
    client: "Judith Criado",
    duration: "3 weeks"
  },
  {
    id: 3,
    url: '/img/easy/',
    img: 'easy.jpg',
    anchor: 'easy',
    class: 'easy-card',
    date: "05/01/2021",
    short_title: "Easy Distribution",
    short_desc: "Showcase website",
    description: "The Easy Distribution project aims to promote Firstseller's capabilities in logistics service and company development. So I wanted a clean design on red tones. But also images of the industrial field to remind the field of activity.",
    client: "SDK - Firstseller",
    duration: "10 weeks"
  },
  {
    id: 4,
    url: '/img/handy/',
    img: 'handy.jpg',
    anchor: 'handy',
    class: 'handy-card',
    date: "03/01/2021",
    short_title: "Handy Location",
    short_desc: "Sports equipment rental application",
    description: 'The concept of the company is composed of two main axes, namely the rental of materials & sports equipment belonging to the company itself, but also the rental of equipment from individuals using HandYLocation as a rental intermediary. The customer will have a mobile application to manage his rentals and to consult the product catalog. But the equipment is only available in store, to be picked up and returned on the spot.',
    client: "Clément Kolodziejczak",
    duration: "8 weeks"
  },
  {
    id: 5,
    url: '/img/sentier/',
    img: 'sentier.jpg',
    anchor: 'sentier',
    class: 'sentier-card',
    date: "15/04/2020",
    short_title: "Sentier Sauvage",
    short_desc: "Showcase website",
    description: "The site sentier-sauvage.fr offers atypical hikes accompanied by experienced guides and naturalists. experienced naturalists. These walks in mountain have for the observation of the Pyrenean biodiversity. For two years, the site has been offering to a varied public the discovery of the stag's bellow.",
    client: "Ugo Bagnarosa",
    duration: "5 weeks"
  }
];
