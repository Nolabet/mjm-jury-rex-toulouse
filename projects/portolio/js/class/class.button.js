class Button {
  constructor(node) {
    this.button = node;
    this.distance = 80;
    this.a = 160;
    this.mouseHasEntered = false;
    this.mouseIsInButtonTerritory = false;
    this.init();
    this.handleEvent();
  }

  init() {

    // Déclaration des variables
    let {
      width,
      height,
      x: centerPointX,
      y: centerPointY
    } = this.button.getBoundingClientRect(); // donne la largeur, la hauteur, l'axe gauche-x, haut-y du bouton

    centerPointX = centerPointX + width / 2; //  point central du bouton sur l'axe X
    centerPointY = centerPointY + height / 2; //  point central du bouton sur l'axe Y

    this.centerPointX = centerPointX;
    this.centerPointY = centerPointY;
  }

  handleEvent() {
    window.addEventListener('mousemove', (e) => this.handleMove(e));
    window.addEventListener('mouseout', () => this.handleReset())
    window.addEventListener('scroll', () => this.init()); // met à jour la position x,y du bouton, pour le bouton sous le viewport
    buttonObjects.push({
      button: this.button,
      isHovered: this.mouseIsInButtonTerritory
    });
  }

  handleMove(e) {
    const x = e.x; // X actuel du curseur
    const y = e.y; // Y actuel du curseur

    const leftBorderLine = this.centerPointX - this.distance;
    const rightBorderLine = this.centerPointX + this.distance;
    const topBorderLine = this.centerPointY - this.distance;
    const bottomBorderline = this.centerPointY + this.distance;
    this.xWalk = (x - this.centerPointX) / 2; // la distance à parcourir pour déplacer le bouton lorsque la souris se déplace sur l'axe X
    this.yWalk = (y - this.centerPointY) / 2; // la distance à parcourir pour déplacer le bouton lorsque la souris se déplace sur l'axe Y

    this.mouseIsInButtonTerritory =
      x > leftBorderLine &&
      x < rightBorderLine &&
      y > topBorderLine &&
      y < bottomBorderline; // devient vrai si la souris se trouve à l'intérieur de toutes ces lignes limites

    if (this.mouseIsInButtonTerritory) {
      if (!this.mouseHasEntered) {
        // ceci ne doit se produire qu'une seule fois pour créer une limite extérieure.
        // créer un autre niveau de frontière en augmentant la distance ;
        // pendant que le curseur revient, le bouton sort de la ligne de démarcation la plus proche et revient de cette ligne de démarcation.
        this.distance = 340;
        this.mouseHasEntered = true;
      }
      this.handleCatch(); // lorsque la souris entre dans le territoire du bouton

    } else {
      this.handleReset()
    }

    const index = buttonObjects.findIndex(button => button.button === this.button);
    buttonObjects[index].isHovered = this.mouseIsInButtonTerritory;
  }

  handleCatch() {

    // déplace le bouton dans la direction où se trouve le curseur.
    this.button.style.transform = "scale(2)";
    this.button.style.transform = `translate(${this.xWalk}px, ${this.yWalk}px)`;
    if ((this.xWalk > 30 || this.yWalk > 30) || (this.xWalk < -30 || this.yWalk < -30)) {
      console.log("yes");
    }
  }

  handleReset() {

    // réinitialise la position du bouton telle qu'elle était initialement.
    this.button.style.transform = "scale(1)";
    this.button.style.transform = `translate(${0}px, ${0}px)`;
    if (this.mouseHasEntered) this.distance = 80;
    this.mouseHasEntered = false;
    // lorsque le bouton revient à sa position (mouseHasEntered = true),
    // cela permet d'augmenter la limite initiale du bouton pour la prochaine fois.
  }
}

function handleBubble(e) {
  bubble.style.left = `${e.x}px`;
  bubble.style.top = `${e.y}px`;

  const hasAnyButtonHovered = buttonObjects.some(buttonObj => buttonObj.isHovered);
  if (hasAnyButtonHovered || e.target.classList.contains("nav__link")) {
    bubble.classList.add("bubble--big");
    document.body.style.cursor = '-webkit-grab';
  } else {
    bubble.classList.remove("bubble--big");
    document.body.style.cursor = 'auto';
  }
}

const buttons = document.querySelectorAll('.shootingStar');
const bubble = document.querySelector('.bubble');
const buttonObjects = [];

buttons.forEach(button => {
  const node = button.querySelector('.shootingStar__content');
  new Button(node);
});

window.addEventListener("mousemove", (e) => handleBubble(e));
