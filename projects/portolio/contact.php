<!DOCTYPE html>
<html>

  <head>
    <?php require_once('partials/head.php'); ?>
  </head>

  <style>
    @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400');
  </style>

  <body>

    <main class="content">
      <span class="bubble"></span>

      <?php require_once('partials/menu.php'); ?>
      <section class="banner" id="contact">
        <h1 class="myname hand-writting text-center" data-aos="fade-left" data-aos-duration="800">Contact me</h1>
        <ul>
          <li data-aos="fade-down-left" data-aos-duration="800" data-aos-delay="200">
            <a href="https://www.facebook.com/nicolas.blet.14" target="_blank">
              <!-- <i class="fa fa-facebook" aria-hidden="true"></i> -->
              <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-facebook" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <path d="M7 10v4h3v7h4v-7h3l1 -4h-4v-2a1 1 0 0 1 1 -1h3v-4h-3a5 5 0 0 0 -5 5v2h-3" />
              </svg>
              <span> - Facebook</span>
            </a>
          </li>
          <li data-aos="fade-down-left" data-aos-duration="800" data-aos-delay="300">
            <a href="https://www.linkedin.com/in/nicolas-blet" target="_blank">
              <!-- <i class="fa fa-twitter" aria-hidden="true"></i> -->
              <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-linkedin" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <rect x="4" y="4" width="16" height="16" rx="2" />
                <line x1="8" y1="11" x2="8" y2="16" />
                <line x1="8" y1="8" x2="8" y2="8.01" />
                <line x1="12" y1="16" x2="12" y2="11" />
                <path d="M16 16v-3a2 2 0 0 0 -4 0" />
              </svg>
              <span> - LinkedIn</span>
            </a>
          </li>
          <li data-aos="fade-down-left" data-aos-duration="800" data-aos-delay="400">
            <a href="mailto:nicolasblet47@gmail.com">
              <!-- <i class="fa fa-google-plus" aria-hidden="true"></i> -->
              <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-mail" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <rect x="3" y="5" width="18" height="14" rx="2" />
                <polyline points="3 7 12 13 21 7" />
              </svg>
              <span> - Mail</span>
            </a>
          </li>
          <li data-aos="fade-down-left" data-aos-duration="800" data-aos-delay="500">
            <a href="https://www.instagram.com/nolab_creative/" target="_blank">
              <!-- <i class="fa fa-instagram" aria-hidden="true"></i> -->
              <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-instagram" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <rect x="4" y="4" width="16" height="16" rx="4" />
                <circle cx="12" cy="12" r="3" />
                <line x1="16.5" y1="7.5" x2="16.5" y2="7.501" />
              </svg>
              <span> - Instagram</span>
            </a>
          </li>
      </section>

    </main>

  </body>

  <!-- JavaScript Bundle with Popper -->
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

  <!-- Personnal JS -->
  <script src="./js/data-projects.js" charset="utf-8"></script>
  <script src="./js/scripts.js" charset="utf-8"></script>
  <script src="./js/class/class.button.js" charset="utf-8"></script>

</html>
