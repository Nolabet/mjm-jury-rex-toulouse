<!DOCTYPE html>
<html>

  <head>
    <?php require_once('partials/head.php'); ?>
  </head>

  <body>

    <main class="content">
      <span class="bubble"> </span>

      <?php require_once('partials/menu.php'); ?>

      <!-- Hero -->
      <section class="banner col-2" id="animation_circle">
        <div class="col-one flex">
          <div class="circle circle-xl" id="circle-main" data-aos="zoom-in" data-aos-duration="800"></div>
          <div class="myself-img" id="circle-img" data-aos="zoom-out" data-aos-duration="800" data-aos-delay="300">
            <!-- Photo de profil -->
          </div>
        </div>
        <div class="col-two">
          <h1 class="myname hand-writting" data-aos="fade-left" data-aos-duration="800">Hello</h1>
          <p data-aos="fade-left" data-aos-duration="800">My name is <strong>Nicolas Blet</strong>.
            I am a designer with 4 years of experience in the digital field. I started with studies in Computer Science which allowed me to acquire skills in web development. Then I continued my path on the other side of Webdesign, namely UX/UI, having previously trained as a Graphic Designer, print and digital.</p>
          <a class="solid btn btn-primary mt-4" href="#summary" data-aos="fade-left" data-aos-duration="800">check <span class="outline">My</span>projects</a>
        </div>
      </section>

      <!-- Header -->
      <section class="wave" id="summary">
        <svg viewBox="0 0 300 300" preserveAspectRatio="xMinYMin meet">
          <path d="M0,100 C150,100 250, 50 500,200 L500,00 L0,0 Z" style="stroke: none; fill:#f5f5f5;"></path>
        </svg>
      </section>

      <section class="project__view" id="projects"></section>

    </main>

    <footer></footer>

  </body>

  <!-- JavaScript Bundle with Popper -->
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

  <!-- Personnal JS -->
  <script src="./js/data-projects.js" charset="utf-8"></script>
  <script src="./js/scripts.js" charset="utf-8"></script>
  <script src="./js/class/class.button.js" charset="utf-8"></script>

</html>
