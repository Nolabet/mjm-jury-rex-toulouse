<!-- header -->
<header class="header">
  <div class="menu">
    <button class="nav-tgl" type="b" aria-label="toggle menu">
      <!--hamburger button--><span aria-hidden="true"></span>
    </button>
    <nav class="nav">
      <ul class="item-list solid">
        <li><a href="../../index.php">Home</a></li>
        <li><a href="portfolio.php"><span class="outline">My</span>portfolio
          </a></li>
        <li><a href="contact.php">Contact<span class="outline">Me</span></a></li>
      </ul>
    </nav>
  </div>
</header>
