<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Projet de fin d'étude MJM GRAPHIC DESIGN 2021" />
    <meta name="author" content="Nicolas Blet" />
    <title>MJM • Jury final</title>

    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />

    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />

    <link href="css/styles.css" rel="stylesheet" />
  </head>

  <body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm" id="mainNav">
      <div class="container px-5">
        <a class="navbar-brand fw-bold" href="#page-top">I'm a creative Designer</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="bi-list"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ms-auto me-4 my-3 my-lg-0">
            <!-- <li class="nav-item"><a class="nav-link me-lg-3" href="./projects/rex/home.php" target="_blank">Le Rex</a></li> -->
            <li class="nav-item"><a class="nav-link me-lg-3" href="./projects/portolio/portfolio.php"><strong>My Portfolio 2021</strong></a></li>
            <li class="nav-item"><a class="nav-link me-lg-3" href="https://issuu.com/nicolasblet/docs/bletnicolas_issuu" target="_blank">Older works</a></li>
          </ul>
          <a class="btn solid btn-primary rounded-pill px-3 mb-2 mb-lg-0" href="./projects/portolio/contact.php">
            <span class="d-flex align-items-center">
              <i class="bi-chat-text-fill me-2"></i>
              <span class="small">Send Feedback</span>
            </span>
          </a>
          <!-- <button class="btn solid btn-primary rounded-pill px-3 mb-2 mb-lg-0" data-bs-toggle="modal" data-bs-target="#feedbackModal">
            <span class="d-flex align-items-center">
              <i class="bi-chat-text-fill me-2"></i>
              <span class="small">Send Feedback</span>
            </span>
          </button> -->
        </div>
      </div>
    </nav>
    <!-- Mashead header-->
    <header class="masthead">
      <div class="container px-5">
        <div class="row gx-5 align-items-center">
          <div class="col-lg-6">
            <!-- Mashead text and app badges-->
            <div class="mb-5 mb-lg-0 text-center text-lg-start">
              <h1 class="mb-3 outline">The world,<br /><span class="solid">after pandemic</span></h1>
              <p class="lead fw-normal text-muted mb-5">For my graduation project, I redesigned the website of the Toulouse theater Le Rex, on the occasion of the reopening of theaters in France.</p>
              <div class="d-flex flex-column flex-lg-row align-items-center">
                <a class="btn solid btn-primary py-3 px-4 rounded-pill" href="https://gitlab.com/Nolabet/mjm-jury-rex-toulouse" target="_blank">Gitlab source code</a>
                <!-- <a class="me-lg-3 mb-4 mb-lg-0" href="#!"><img class="app-badge" src="assets/img/google-play-badge.svg" alt="..." /></a>
              <a href="#!"><img class="app-badge" src="assets/img/app-store-badge.svg" alt="..." /></a> -->
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <!-- Masthead device mockup feature-->
            <div class="masthead-device-mockup">
              <svg class="circle" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <defs>
                  <linearGradient id="circleGradient" gradientTransform="rotate(45)">
                    <stop class="gradient-start-color" offset="0%"></stop>
                    <stop class="gradient-end-color" offset="100%"></stop>
                  </linearGradient>
                </defs>
                <circle cx="50" cy="50" r="50"></circle>
              </svg><svg class="shape-1 d-none d-sm-block" viewBox="0 0 240.83 240.83" xmlns="http://www.w3.org/2000/svg">
                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(120.42 -49.88) rotate(45)"></rect>
                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(-49.88 120.42) rotate(-45)"></rect>
              </svg><svg class="shape-2 d-none d-sm-block" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <circle cx="50" cy="50" r="50"></circle>
              </svg>
              <div class="device-wrapper">
                <div class="device" data-device="iPhoneX" data-orientation="portrait" data-color="black">
                  <div class="screen bg-black">
                    <!-- PUT CONTENTS HERE:-->
                    <!-- * * This can be a video, image, or just about anything else.-->
                    <!-- * * Set the max width of your media to 100% and the height to-->
                    <!-- * * 100% like the demo example below.-->
                    <img src="assets/img/home-rex.jpg" style="max-width: 100%; height: 100%" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- Quote/testimonial aside-->
    <aside class="text-center bg-gradient-dark-to-blue">
      <div class="container px-5">
        <div class="row gx-5 justify-content-center">
          <div class="col-xl-8">
            <div class="h3 fs-1 text-white mb-4">"All the world's a stage, and all the men and women merely players; They have their exits and their entrances; And one man in his time plays many parts"</div>
            <p class="text-white">William Shakespeare</p>
          </div>
        </div>
      </div>
    </aside>
    <!-- App features section-->
    <section id="features">
      <div class="container px-5">
        <div class="row gx-5 align-items-center">
          <div class="col-lg-8 order-lg-1 mb-5 mb-lg-0">
            <div class="container-fluid px-5">
              <div class="row gx-5">
                <div class="col-md-6 mb-5">
                  <!-- Feature item-->
                  <div class="text-center">
                    <i class="bi-phone icon-feature text-gradient d-block mb-3"></i>
                    <h3 class="font-alt">Responsive Design</h3>
                    <p class="text-muted mb-0">Just with HTML/CSS, my design<br>is available on mobile.</p>
                  </div>
                </div>
                <div class="col-md-6 mb-5">
                  <!-- Feature item-->
                  <div class="text-center">
                    <i class="bi bi-snow2 icon-feature text-gradient d-block mb-3"></i>
                    <h3 class="font-alt">Atomic Design</h3>
                    <p class="text-muted mb-0">Atomic design is methodology for creating design systems with distinct levels.</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 mb-5 mb-md-0">
                  <!-- Feature item-->
                  <div class="text-center">
                    <i class="bi bi-person-check icon-feature text-gradient d-block mb-3"></i>
                    <h3 class="font-alt">Thinking for users</h3>
                    <p class="text-muted mb-0">The right information,<br> in the right place!</p>
                  </div>
                </div>
                <div class="col-md-6">
                  <!-- Feature item-->
                  <div class="text-center">
                    <i class="bi-patch-check icon-feature text-gradient d-block mb-3"></i>
                    <h3 class="font-alt">Open Source</h3>
                    <p class="text-muted mb-0">This is a small project, <br>but my code is available on gitlab.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 order-lg-0">
            <!-- Features section device mockup-->
            <div class="features-device-mockup">
              <svg class="circle" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <defs>
                  <linearGradient id="circleGradient" gradientTransform="rotate(45)">
                    <stop class="gradient-start-color" offset="0%"></stop>
                    <stop class="gradient-end-color" offset="100%"></stop>
                  </linearGradient>
                </defs>
                <circle cx="50" cy="50" r="50"></circle>
              </svg><svg class="shape-1 d-none d-sm-block" viewBox="0 0 240.83 240.83" xmlns="http://www.w3.org/2000/svg">
                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(120.42 -49.88) rotate(45)"></rect>
                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(-49.88 120.42) rotate(-45)"></rect>
              </svg><svg class="shape-2 d-none d-sm-block" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <circle cx="50" cy="50" r="50"></circle>
              </svg>
              <div class="device-wrapper">
                <div class="device" data-device="iPhoneX" data-orientation="portrait" data-color="black">
                  <div class="screen bg-black">
                    <!-- PUT CONTENTS HERE:-->
                    <!-- * * This can be a video, image, or just about anything else.-->
                    <!-- * * Set the max width of your media to 100% and the height to-->
                    <!-- * * 100% like the demo example below.-->
                    <img src="assets/img/menu-mobile.jpg" style="max-width: 100%; height: 100%" alt="">

                    <!-- <video muted="muted" autoplay="" loop="" style="max-width: 100%; height: 100%">
                      <source src="assets/img/demo-screen.mp4" type="video/mp4" /></video> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Basic features section-->
    <section class="bg-light">
      <div class="container px-5">
        <div class="row gx-5 align-items-center justify-content-center justify-content-lg-between">
          <div class="col-12 col-lg-5">
            <h2 class="mb-4">Visit my portfolio!</h2>
            <p class="lead fw-normal text-muted mb-5 mb-lg-0">Are you interested in my project? Discover my other works! I had the opportunity to work on web projects and mobile application projects
              with radically different artistic directions. Have a nice reading!</p>
            <div class="d-flex flex-column flex-lg-row align-items-center mt-5">
              <a class="btn solid btn-primary py-3 px-4 rounded-pill" href="./projects/portolio/portfolio.php" target="_blank"><span class="outline">My</span>Portfolio</a>
            </div>
          </div>
          <div class="col-sm-8 col-md-6">
            <div class="px-5 px-sm-0"><img class="img-fluid rounded-circle" src="projects/portolio/img/tattoo/album-7.jpg" alt="..." /></div>
          </div>
        </div>
      </div>
    </section>
    <!-- Call to action section-->
    <section class="cta">
      <div class="cta-content">
        <div class="container px-5">
          <h2 class="text-white display-1 lh-1 mb-4">
            Stop waiting.
            <br />
            Start building.
          </h2>
          <a class="btn solid btn-outline-light py-3 px-4 rounded-pill" href="https://youtu.be/EC7qPuM4dOI" target="_blank">Play video</a>
        </div>
      </div>
    </section>
    <!-- App badge section-->
    <section class="bg-gradient-purle" id="download">
      <div class="container px-5">
        <h2 class="text-center text-white font-alt mb-4">Thanks for watching!</h2>
        <div class="d-flex flex-column flex-lg-row align-items-center justify-content-center">
          <!-- <a class="me-lg-3 mb-4 mb-lg-0" href="#!"><img class="app-badge" src="assets/img/google-play-badge.svg" alt="..." /></a>
        <a href="#!"><img class="app-badge" src="assets/img/app-store-badge.svg" alt="..." /></a> -->
        </div>
      </div>
    </section>
    <!-- Footer-->
    <footer class="bg-black text-center py-5">
      <div class="container px-5">
        <div class="text-white-50 small">
          <div class="mb-2">&copy; nolab_creative 2021 • Portfolio. All Rights Reserved.</div>
          <!-- <a href="#!">Privacy</a>
        <span class="mx-1">&middot;</span>
        <a href="#!">Terms</a>
        <span class="mx-1">&middot;</span>
        <a href="#!">FAQ</a> -->
        </div>
      </div>
    </footer>
    <!-- Feedback modal-->
    <div class="modal fade" id="feedbackModal" tabindex="-1" aria-labelledby="feedbackModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header bg-gradient-primary-to-secondary p-4">
            <h5 class="modal-title font-alt" id="feedbackModalLabel">Send feedback</h5>
            <button class="btn-close btn-close-black" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body border-0 p-4">
            <form method="post" action="mail.php">
              <div class="form-floating mb-4">
                <input class="form-control" id="inputName" type="text" placeholder="Enter your name..." />
                <label for="inputName">Name</label>
              </div>
              <div class="form-floating mb-4">
                <input class="form-control" id="inputEmail" type="email" placeholder="name@example.com" />
                <label for="inputEmail">Email address</label>
              </div>
              <!-- <div class="form-floating mb-4">
                <input class="form-control" id="inputPhone" type="tel" placeholder="(123) 456-7890" />
                <label for="inputPhone">Phone number</label>
              </div> -->
              <div class="form-floating mb-4">
                <textarea class="form-control" id="inputMessage" name="message" placeholder="Enter your message here..." style="height: 10rem"></textarea>
                <label for="inputMessage">Message</label>
              </div>
              <div class="d-grid"><button class="btn solid btn-primary rounded-pill py-3" type="submit">Submit</button></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
  </body>

</html>
