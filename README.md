# mjm-jury-rex-toulouse

Après des mois de fermeture, les lieux culturels peuvent ré-ouvrir leurs portes au public. Étant donné la situation sanitaire, le secteur doit composer avec des contraintes de couvre-feu et de capacités de jauge mais a la ferme intention d'accueillir de nouveau le public pour faire revivre une activité en berne depuis des mois.

Vous réaliserez une **page web** pour la **réouverture au public** d'un lieu culturel toulousain qui propose du spectacle vivant. La **page d'accueil devra présenter le protocole d'accueil** selon le lieu et l'activité, es conditions d'accès au public, les aménagements prévus, les propositions de spectacle alternatives et quelques éléments de programmation. Le lieu est au choix, il pourra s'agir d'une salle de concert, de spectacle, ou d'un théâtre.

**Ex :** Le Bikini, Le Métronum, Le Théatre Garonne, Le Théâtre Sorano...

**3min** par partie
